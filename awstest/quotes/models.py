from django.db import models
from datetime import datetime

# Create your models here.
class Quote(models.Model):
    quote = models.CharField(max_length=255)
    pub_date = models.DateTimeField('Date entered', 
        auto_now_add=True,
        editable='False')