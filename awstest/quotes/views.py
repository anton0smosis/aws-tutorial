from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse

from quotes.models import Quote

# Create your views here.
def index(request):
    rdm_quote = Quote.objects.order_by('?').first()
    template = loader.get_template('quotes/index.html')
    context = {
        'random_quote' : rdm_quote
    }
    return HttpResponse(template.render(context, request))

def add_quote(request):
    text = Quote(quote=request.POST['quote'])
    print("Text will be: " + text.quote)
    if text != "":
        new_quote = text
        new_quote.save()
        template = loader.get_template('quotes/index.html')
        context = {
            'random_quote' : new_quote
        }
        return HttpResponse(template.render(context, request))
    else:
        return render(request, 'quotes/index.html', {
            'error_message': "Please enter a quote"
        })
    