from django.urls import path

from .views import index, add_quote

app_name = 'quotes'
urlpatterns = [
    path('', index, name='index'),
    path('add/', add_quote, name='add'),
]